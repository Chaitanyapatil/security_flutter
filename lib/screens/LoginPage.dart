import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_security_app/screens/RegisterPage.dart';
import 'package:flutter_security_app/screens/VisitorList.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  TextEditingController emailInputController;
  TextEditingController pwdInputController;
  bool loading = false;

  @override
  initState() {
    emailInputController = new TextEditingController();
    pwdInputController = new TextEditingController();
    super.initState();
  }

  String emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Email format is invalid';
    } else {
      return null;
    }
  }

  String pwdValidator(String value) {
    if (value.length < 8) {
      return 'Password must be longer than 8 characters';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: pageToDisplay());
  }

  void navigateToRegisterPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return RegisterPage();
    }));
  }

  static void showToast(String message) {
    Fluttertoast.showToast(
      backgroundColor: Colors.grey,
      msg: message,
      toastLength: Toast.LENGTH_LONG,
    );
  }

  Widget loader() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget mainPage() {
    return Container(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
            child: Form(
          key: _loginFormKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Email*', hintText: "john.doe@gmail.com"),
                controller: emailInputController,
                keyboardType: TextInputType.emailAddress,
                validator: emailValidator,
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: 'Password*', hintText: "********"),
                controller: pwdInputController,
                obscureText: true,
                validator: pwdValidator,
              ),
              RaisedButton(
                child: Text("Login"),
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  setState(() {
                    if (_loginFormKey.currentState.validate()) {
                      loading = true;

                      FirebaseAuth.instance
                          .signInWithEmailAndPassword(
                            email: emailInputController.text,
                            password: pwdInputController.text,
                          )
                          .then((currentUser) => Firestore.instance
                              .collection("users")
                              .document(currentUser.uid)
                              .get()
                              .then((DocumentSnapshot result) => {
                                    loading = false,
                                    Navigator.pushReplacement(context,
                                        MaterialPageRoute(builder: (context) {
                                      return VisitorList(
                                        result["fname"],
                                        currentUser.uid,
                                      );
                                    }))
                                  })
                              .catchError((err) => print(err)))
                          .catchError((err) {
                        setState(() {
                          loading = false;
                        });
                        showToast("Username or Password is Incorrect");
                      });
                    }
                  });
                },
              ),
              Text("Don't have an account yet?"),
              FlatButton(
                child: Text("Register here!"),
                onPressed: () {
                  navigateToRegisterPage();
                },
              )
            ],
          ),
        )));
  }

  Widget pageToDisplay() {
    if (loading) {
      return loader();
    } else {
      return mainPage();
    }
  }
}
