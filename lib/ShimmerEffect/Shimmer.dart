import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'ShimmerLayout.dart';


class ShimmerUi extends StatefulWidget {
  @override
  _ShimmerUiState createState() => _ShimmerUiState();
}

class _ShimmerUiState extends State<ShimmerUi> {
  @override
  Widget build(BuildContext context) {

    int offset = 0;
    int time = 800;


    return Scaffold(
      appBar: AppBar(title: Text(' Visitor List'),),
      body: SafeArea(
          child: ListView.builder(
              itemCount: 6,
              itemBuilder: (BuildContext context, index){

                offset += 5;
                time = 600 + offset;

                print(time);

                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Shimmer.fromColors(
                      baseColor:Colors.grey[300],
                      highlightColor: Colors.white,
                      child: ShimmerLayout(),
                      period: Duration(milliseconds: time),
                    )
                );
              })
      ),
    );
  }
}