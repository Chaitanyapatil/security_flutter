import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_security_app/ShimmerEffect/Shimmer.dart';
import 'package:flutter_security_app/screens/CustomCard.dart';
import 'package:flutter_security_app/screens/LoginPage.dart';
import 'package:flutter_security_app/screens/VisitorDetail.dart';

class VisitorList extends StatefulWidget {
  VisitorList(this.title, this.uid);

  final String title;
  final String uid;

  @override
  State<StatefulWidget> createState() {
    return VisitorListState(this.title, this.uid);
  }
}

class VisitorListState extends State<VisitorList> {
  var title;
  FirebaseUser currentUser;
  var uid;

  VisitorListState(this.title, this.uid);

  int count = 10;
  DocumentSnapshot _currentDocument;
  @override
  void initState() {
    this.getCurrentUser();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(' Visitor List'),
        actions: <Widget>[
          FlatButton(
            child: Text("Log Out"),
            textColor: Colors.white,
            onPressed: (){
              FirebaseAuth.instance
              .signOut()
                  .then((result) =>
              navigateToLogIn())
                  .catchError((err) => print(err));
              },
          )
        ],
      ),
      body:
      getListData(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          debugPrint('fab clicked');
          navigateToDetail();
        },
        tooltip: 'Add Visitor',
        child: Icon(Icons.add),
      ),
    );
  }

  void navigateToDetail() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return VisitorDetail(title: title, uid: uid);
    }));
  }

// THE MAIN CREATION OF CLASS IS HERE, IT SHOULD BE IN THE BODY
 Widget getListData() {
   return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection("users")
          .document(widget.uid)
          .collection('Visitors')
          .orderBy('dateAndTime', descending: true)
          .snapshots(),
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return new ShimmerUi();
          default:
            return new ListView(
              padding: EdgeInsets.all(8),
              children: snapshot.data.documents
                  .map((DocumentSnapshot document) {
                return new CustomCard(
                  // IT CREATES CUSTOME CARD AND SHOULD DO CODE IN IT TO RETRIVE DATA FROM DATABASE
                  widget: widget.uid,
                  title:document['visitorName'],
                  description:document['visitorMobileNo'],
                  roomNo: document['hostRoomNo'],
                  visitorNo: document['noOfVisitors'],
                  noOfchild: document['noOfChilds'],
                  vehicle: document['vehicleTypeValue'],
                  vehicleNo: document['vehicleNo'],
                  visitingPurpose: document['visitingPurpose'],
                  otherPurpose: document['OtherPurpose'],
                  url: document['url'],
                  time: document['dateAndTime'],
                  outTime: document['outTime'],
                    document: document,


                );
              }).toList(),
            );
        }
      },
    );
  }

  void getCurrentUser() async {
    var currentUser = await FirebaseAuth.instance.currentUser();
    debugPrint(currentUser.email);
  }

  navigateToLogIn() {
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return LoginPage();
    }));
  }
}
