import 'package:flutter/material.dart';
import 'package:flutter_security_app/screens/VisitorDetail.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';

class TaskPage extends StatelessWidget {
  TaskPage({@required this.title, this.description, this.roomNo, this.visitorNo, this.vehicle, this.vehicleNo,
  this.visitingPurpose, this.otherPurpose, this.url, this.time, this.outTime, this.document, this.noOfchild});

  final title;
  final description;
  final roomNo;
  final visitorNo;
  final vehicle;
  final vehicleNo;
  final visitingPurpose;
  final otherPurpose;
  var url;
  var time;
  var outTime;
  var document;
  var noOfchild;


  bool isOtherPurposeVisible = true;
  final notAvailable = "N/A";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Visitor Details"),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
          child: SingleChildScrollView(
            child: Container(
              child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _addImageUrlUi(),
                      _addRoomNoUi(),
                      _addNameUi(),
                      _addMobNoUi(),
                      _addNoOfVisitorsUi(),
                      _addnoOfchild(),
                      _addVehicleUi(),
                      _addVehicleNoUi(),
                      _addVisitingPurposeUi(),
                      _addOtherPurposeUi(),
                      _addTimeUi(),
                      _addOutTimeUi(),
                      _addButtonUi(context),

                    ]),
              ),
            ),
          ),
        ));
  }

  Widget _addRoomNoUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Room No: $roomNo",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),
    );
  }

  Widget _addNameUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Visitor Name: $title",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),
    );
  }

  Widget _addMobNoUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Visitor MobNo: $description",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),
    );
  }

  Widget _addNoOfVisitorsUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("No Of Visitors: ${visitorNo}",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),
    );
  }

  Widget _addVehicleUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Visitor Vehicle: ${vehicle == null ? notAvailable : vehicle}",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),
    );
  }

  Widget _addVehicleNoUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Vehicle No: ${vehicleNo.toString().isEmpty ? notAvailable : vehicleNo}",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),
    );
  }

  Widget _addVisitingPurposeUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Visiting Purpose: ${visitingPurpose}",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),

    );
  }

  Widget _addOtherPurposeUi() {
    return Visibility(
      visible: isOtherPurposeVisible,
      child: Card(
        elevation: 5.0,
        shape:RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)
        ),
        child: SizedBox(
          height:50,
          width: double.infinity,
          child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
            child: Text("Other Purpose: ${otherPurpose.toString().isEmpty ? notAvailable : otherPurpose }",
                style: new TextStyle(
                  fontSize: 15.0,
                )),
          ),
        ),
      ),
    );
  }

  Widget _addButtonUi(context) {
    return Padding(
      padding:EdgeInsets.only(top:5.0, bottom: 10.0),
      child: SizedBox(
        height: 50,
        width: double.infinity,
        child: RaisedButton(
            child: Text('Back To HomeScreen',
              style: TextStyle(
                  fontSize: 15.0
              ),),
            color: Theme.of(context).primaryColor,
            textColor: Colors.white,
            elevation: 10.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            onPressed: () => Navigator.pop(context)),
      ),
    );
  }

  Widget _addImageUrlUi() {
    return
      Padding(
        padding: EdgeInsets.only(bottom: 5.0),
        child: CircularProfileAvatar(
        "$url",
        radius: 80,
        borderWidth: 5,
        borderColor: Colors.deepPurpleAccent,
        elevation: 10.0,
        cacheImage: true,
    ),
      );

  }

  Widget _addTimeUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("In Time: $time",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),

    );
  }

  Widget _addOutTimeUi() {
    return Card(
      elevation: 5.0,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
      ),
      child: SizedBox(
        height:50,
        width: double.infinity,
        child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
          child: Text("Out Time: ${outTime.toString().isEmpty ? notAvailable : outTime}",
              style: new TextStyle(
                fontSize: 15.0,
              )),
        ),
      ),

    );
  }

 Widget _addnoOfchild() {
   return Card(
     elevation: 5.0,
     shape:RoundedRectangleBorder(
         borderRadius: BorderRadius.circular(10.0)
     ),
     child: SizedBox(
       height:50,
       width: double.infinity,
       child: Padding(padding: EdgeInsets.only(top: 15.0, left: 10.0),
         child: Text("No Of Child: ${noOfchild == null ? notAvailable : noOfchild}",
             style: new TextStyle(
               fontSize: 15.0,
             )),
       ),
     ),

   );
 }

}
