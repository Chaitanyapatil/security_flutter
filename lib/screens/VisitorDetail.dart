import 'dart:io';
import 'dart:math';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_security_app/screens/image_picker_handler.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:intl/intl.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';


class VisitorDetail extends StatefulWidget {
  VisitorDetail({Key key, this.title, this.uid})
      : super(key: key); //update this to include the uid in the constructor
  final String title;
  final String uid;

  @override
  State<StatefulWidget> createState() => VisitorDetailState(uid);
}

class VisitorDetailState extends State<VisitorDetail> with TickerProviderStateMixin , ImagePickerListener {
  String url;
  String currentText = "";
  List<String> added = [];

  VisitorDetailState(String uid) {
    this.uid = uid;
  }

  String uid;
  final time =
      new DateFormat("dd-MM-yyyy hh:mm a").format(DateTime.now()).toString();
  String outTime = "";



  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;

  var _formKey = GlobalKey<FormState>();

  bool isVehicleTypeVisible = false;
  bool isVisiblityOn = false;
  bool isVisitorPurposeVisible = false;
  bool isPurposeErrorVisible = false;
  bool isLoading = false;
  bool adultErrorVisible = false;
  bool rooNoErrorVisible = false;

  TextEditingController visitornameController = TextEditingController();
  TextEditingController vehicleNoController = TextEditingController();
  TextEditingController visitorMobNoController = TextEditingController();
  TextEditingController OtherPurposeController = TextEditingController();
  TextEditingController addRoomNOController = TextEditingController();

  FirebaseUser currentUser;
  String vehicleTypeValue;
  String visitorPurposeValue;
  String childVisitorNo;
  String adultVisitorNo;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),

    );

    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.init();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    return WillPopScope(
      /*onWillPop: (){
        moveToLastScreen();
      },*/
      child: Scaffold(
        appBar: AppBar(
          title: Text('Visitor Detail Form'),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                moveToLastScreen();
              }),
        ),
        body: Form(key: _formKey, child: _pageToDisplay()),
      ),
    );
  }

  Widget addNameUi() {
    return Padding(
      padding: EdgeInsets.only(right: 10, top: 20.0, left: 10),
      child: TextFormField(
        controller: visitornameController,
        validator: _validateName,
        decoration: InputDecoration(
            labelText: 'Name*',
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
  }

  Widget addMobNoUi() {
    return Padding(
      padding: EdgeInsets.only(right: 10, top: 25.0, left: 10),
      child: TextFormField(
        maxLength: 10,
        buildCounter: (BuildContext context,
                {int currentLength, int maxLength, bool isFocused}) =>
            null,
        keyboardType: TextInputType.number,
        controller: visitorMobNoController,
        validator: _validateMobNo,
        decoration: InputDecoration(
            labelText: 'Mob No*',
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      ),
    );
  }

  Widget addVehicleNoUi() {
    return Visibility(
      visible: isVehicleTypeVisible,
      child: Padding(
        padding: EdgeInsets.only(right: 10, top: 25.0, left: 10),
        child: TextFormField(
          controller: vehicleNoController,
          //style: textStyle,
          validator: _validateVehicleNo,
          decoration: InputDecoration(
              labelText: 'Vehicle No*',
              //labelStyle: textStyle,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0))),
        ),
      ),
    );
  }

  Widget addSubmitDeleteUi() {
    return Padding(
      padding: EdgeInsets.only(top: 25.0, bottom: 20, right: 10, left: 10),
      child: SizedBox(
        width: double.infinity,
        height: 55,
        child: RaisedButton(
          color: Theme.of(context).primaryColorDark,
          textColor: Theme.of(context).primaryColorLight,
          child: Text(
            'Save',
            textScaleFactor: 1.5,
          ),
          elevation: 10.0,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0)),
          onPressed: () {
            setState(() {
              if (_image == null) {
                isVisiblityOn = true;
              } else if (_image.path == null) {
                isVisiblityOn = true;
              } else if (_image.path.isEmpty) {
                isVisiblityOn = true;
              } else {
                isVisiblityOn = false;
              }

              if (addRoomNOController.text == null) {
                rooNoErrorVisible = true;
              } else if(addRoomNOController.text.isEmpty){
                rooNoErrorVisible = true;
              } else{
                rooNoErrorVisible = false;
              }

              if (adultVisitorNo == null) {
                adultErrorVisible = true;
              } else {
                adultErrorVisible = false;
              }

              if (visitorPurposeValue == null) {
                isPurposeErrorVisible = true;
              } else {
                isPurposeErrorVisible = false;
              }
              if (!isVisiblityOn) {
                if (!rooNoErrorVisible) {
                  if (!adultErrorVisible) {
                    if (!isPurposeErrorVisible) {
                      if (_formKey.currentState.validate()) {
                        isLoading = true;
                        uploadPic(context);
                      }
                    }
                  }
                }
              }
              //uploadPic(context);
            });
          },
        ),
      ),
    );
  }


  Widget addVehicleUi() {
    return Container(
      margin: EdgeInsets.only(right: 10, top: 25.0, left: 10),
      child: FormField<String>(
        builder: (FormFieldState<String> state) {
          return new InputDecorator(
            decoration: InputDecoration(
                contentPadding:
                    new EdgeInsets.symmetric(vertical: 7.0, horizontal: 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0))),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                hint: new Text(
                  "Select Vehicle",
                ),
                value: vehicleTypeValue,
                isExpanded: true,
                onChanged: (String newValue) {
                  state.didChange(newValue);

                  setState(() {
                    vehicleTypeValue = newValue;
                    _changed(vehicleTypeValue);
                  });
                },
                items: <String>['Bike', 'Car', 'Heavy Vehicle','None']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
              ),
            ),
          );
        },
      ),
    );
  }

  void _changed(String vehicleTypevalue) {
    setState(() {
      if (vehicleTypevalue != null) {
        if (vehicleTypevalue.toLowerCase().contains("none")) {
          isVehicleTypeVisible = false;
        } else {
          isVehicleTypeVisible = true;
        }
      } else {
        isVehicleTypeVisible = true;
      }
    });
  }

  void _changedIn(String visitorPurposeValue) {
    setState(() {
      if (visitorPurposeValue != null) {
        if (visitorPurposeValue.toLowerCase().contains("others")) {
          isVisitorPurposeVisible = true;
        } else {
          isVisitorPurposeVisible = false;
        }
      } else {
        isVisitorPurposeVisible = false;
      }
    });
  }

  @override
  userImage(File _image) {
    setState(() {
      this._image = _image;
    });
  }

  Future uploadPic(BuildContext context) async {
    String filename = (Random().nextInt(100000).toString()+ uid );
    StorageReference FirebaseStorageRef =
        FirebaseStorage.instance.ref().child(filename);
    StorageUploadTask uploadTask = FirebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    url = (await taskSnapshot.ref.getDownloadURL());

    debugPrint(uid);
    addDataInFirebase(uid);
  }

  Widget imageViewUi() {
    return new GestureDetector(
      onTap: () => imagePicker.showDialog(context),
      child: Center(
        child: _image == null ? showEmptyImage() : showFillImage(),
      ),
    );
  }

  void addDataInFirebase(String uid) {
    Firestore.instance
        .collection("users")
        .document(widget.uid)
        .collection("Visitors")
        .add({
          "visitorName": visitornameController.text,
          "hostRoomNo": addRoomNOController.text,
          "vehicleNo": vehicleNoController.text,
          "visitorMobileNo": visitorMobNoController.text,
          "vehicleTypeValue": vehicleTypeValue,
          "noOfVisitors": adultVisitorNo,
          "noOfChilds": childVisitorNo,
          "visitingPurpose": visitorPurposeValue,
          "OtherPurpose": OtherPurposeController.text,
          "url": url,
          "dateAndTime": time,
          "outTime": outTime
        })
        .then((result) => {
              isLoading = false,
              Navigator.pop(context),
              visitornameController.clear(),
              addRoomNOController.clear(),
              vehicleNoController.clear(),
              visitorMobNoController.clear(),
              OtherPurposeController.clear(),
            })
        .catchError((err) => print(err));
  }

  String _validateName(String value) {
    if (value.isEmpty) {
      return 'Please Enter Visitor Name';
    }
  }

  String _validateMobNo(String value) {
    if (value.isNotEmpty) {
      if (value.length < 10) {
        return "Please Enter Correct Mob No";
      }
    } else if (value.isEmpty) {
      return "Enter Visitor Mob No";
    }
  }

  String _validateVehicleNo(String value) {
    if (value.isEmpty) {
      return 'Please Enter Visitor Vehicle No';
    }
  }

  String _validateHostRoomNo(String value) {
    if (value.isEmpty) {
      return 'Please Enter Visitor Host Room No';
    }
  }

  String _validateOtherPurpose(String value) {
    if (value.isEmpty) {
      return 'Please Enter Other Purpose For Visit';
    }
  }

  Widget showEmptyImage() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: new Stack(
            children: <Widget>[
              Center(
                child: new CircleAvatar(
                  radius: 75.0,
                  backgroundColor: Colors.lightBlueAccent,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 37),
                child: Align(
                  alignment: Alignment.center,
                  child: new Center(
                    child: Icon(Icons.camera_alt, size: 80),
                  ),
                ),
              ),
            ],
          ),
        ),
        addErrorText()
      ],
    );
  }

  Widget showFillImage() {
    return Container(
      height: 150.0,
      width: 150.0,
      decoration: new BoxDecoration(
        //color: const Color(0xff7c94b6),
        image: new DecorationImage(
          image: new ExactAssetImage(_image.path),
          fit: BoxFit.cover,
        ),
        border: Border.all(color: Colors.deepPurpleAccent, width: 4.0),
        borderRadius: new BorderRadius.all(const Radius.circular(80.0)),
      ),
    );
  }

  Widget addErrorText() {
    return Visibility(
      visible: isVisiblityOn,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "Please Enter Visitor Image",
          style: TextStyle(color: Colors.red),
        ),
      ),
    );
  }

  void moveToLastScreen() {
    Navigator.pop(context);
  }

  Widget addNoOfAdultVisitors() {
    return Container(
      margin: EdgeInsets.only(right: 10, top: 25.0, left: 10),
      child: FormField<String>(
        builder: (FormFieldState<String> state) {
          return new InputDecorator(
            decoration: InputDecoration(
                contentPadding:
                new EdgeInsets.symmetric(vertical: 7.0, horizontal: 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0))),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                hint: new Text(
                  "Add No Of Adult Visitors",
                ),
                value: adultVisitorNo,
                isExpanded: true,
                onChanged: (String newValue) {
                  state.didChange(newValue);

                  setState(() {
                    adultVisitorNo = newValue;

                  });
                },
                items: <String>['1 Adult Visitor','2 Adult Visitors','3 Adult Visitors','4 Adult Visitors',
                '5 Adult Visitors','6 Adult Visitor','7 Adult Visitor','8 Adult Visitor','9 Adult Visitor','10 Adult Visitor','None']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget addVisitingPurposeUi() {
    return Container(
      margin: EdgeInsets.only(right: 10, top: 25.0, left: 10),
      child: FormField<String>(
        // validator: _validateType,
        builder: (FormFieldState<String> state) {
          return new InputDecorator(
            decoration: InputDecoration(
                contentPadding:
                    new EdgeInsets.symmetric(vertical: 7.0, horizontal: 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0))),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                hint: new Text(
                  "Visiting Purpuse",
                ),
                value: visitorPurposeValue,
                isExpanded: true,
                onChanged: (String newValue) {
                  state.didChange(newValue);

                  setState(() {
                    visitorPurposeValue = newValue;
                    _changedIn(visitorPurposeValue);
                  });
                },
                items: <String>[
                  'Relative',
                  'Courier',
                  'Food Delivery',
                  'Society Work',
                  'Daily Housing Work',
                  'others'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget addOtherPurposeUi() {
    return Visibility(
      visible: isVisitorPurposeVisible,
      child: Padding(
        padding: EdgeInsets.only(right: 10, top: 25.0, left: 10),
        child: TextFormField(
          controller: OtherPurposeController,
          //style: textStyle,
          validator: _validateOtherPurpose,
          decoration: InputDecoration(
              labelText: 'Type Other Purpose For Visit*',
              //labelStyle: textStyle,
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0))),
        ),
      ),
    );
  }

  Widget addPurposeErrorUi() {
    return Visibility(
      visible: isPurposeErrorVisible,
      child: Padding(
        padding: const EdgeInsets.only(right: 131, bottom: 2, top: 7.5),
        child: Text(
          "Please Select Visiting Purpose",
          style: TextStyle(color: Colors.red, fontSize: 12),
        ),
      ),
    );
  }

  Widget addGetTimeUi() {
    return Card(
      child: SizedBox(
        height: 50.0,
        width: double.infinity,
        child: Text("$time"),
      ),
    );
  }

  Widget _addAutocompleteUi() {
    return Padding(
      padding:EdgeInsets.only(right: 10, top: 25.0, left: 10),
      child: SimpleAutoCompleteTextField(
        controller: addRoomNOController,
        decoration: InputDecoration(
          labelText: "Add Room No",
            border:
            OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
        ),
        suggestions: suggestions,
        textChanged: (text) => addRoomNOController = text as TextEditingController,
        submitOnSuggestionTap: true,
        textSubmitted: (text) => setState(() {
              if (text != "") {
                addRoomNOController = text as TextEditingController;
              }
            }),
      ),
    );
  }

  List<String> suggestions = [
    'A1',
    'A2',
    'A3',
    'A4',
    'A5',
    'A6',
    'A7',
    'A8',
    'A9',
    'A10',
    'A11',
    'A12',
    'A13',
    'A14',
    'B1',
    'B2',
    'B3',
    'B4',
    'B5',
    'B6',
    'B7',
    'B8',
    'B9',
    'B10',
    'B11',
    'B12'
  ];

  Widget loader() {
    return Stack(
      children: <Widget>[
        Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  Widget homeView() {
    return Padding(
      padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              imageViewUi(),
              _addAutocompleteUi(),
              addRoomNoErrorUi(),
              addNameUi(),
              addMobNoUi(),
              addNoOfAdultVisitors(),
              addAdultErrorUi(),
              addNoOfChildVisitors(),
              addVehicleUi(),
              addVehicleNoUi(),
              addVisitingPurposeUi(),
              addPurposeErrorUi(),
              addOtherPurposeUi(),
              addSubmitDeleteUi(),
            ],
          ),
        ),
      ),
    );
  }

  _pageToDisplay() {
    if (isLoading) {
      return loader();
    } else {
      return homeView();
    }
  }

  Widget addNoOfChildVisitors() {
    return Container(
      margin: EdgeInsets.only(right: 10, top: 25.0, left: 10),
      child: FormField<String>(
        builder: (FormFieldState<String> state) {
          return new InputDecorator(
            decoration: InputDecoration(
                contentPadding:
                new EdgeInsets.symmetric(vertical: 7.0, horizontal: 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0))),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                hint: new Text(
                  "Add No Of Child Visitors",
                ),
                value: childVisitorNo,
                isExpanded: true,
                onChanged: (String newValue) {
                  state.didChange(newValue);

                  setState(() {
                    childVisitorNo = newValue;
                  });
                },
                items: <String>['1 Child Visitor','2 Child Visitors','3 Child Visitors','4 Child Visitors',
                '5 Child Visitors','None']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget addAdultErrorUi() {
    return Visibility(
      visible: adultErrorVisible,
      child: Padding(
        padding: const EdgeInsets.only(right: 145, bottom: 2, top: 7.5),
        child: Text(
          "Please Select No Of Adults",
          style: TextStyle(color: Colors.red, fontSize: 12),
        ),
      ),
    );
  }

 Widget addRoomNoErrorUi() {
   return Visibility(
     visible: rooNoErrorVisible,
     child: Padding(
       padding: const EdgeInsets.only(right: 180, bottom: 2, top: 7.5),
       child: Text(
         "Please Add Room No",
         style: TextStyle(color: Colors.red, fontSize: 12),
       ),
     ),
   );
 }

}
