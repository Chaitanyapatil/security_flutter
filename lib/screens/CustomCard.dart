import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_security_app/screens/TaskPage.dart';
import 'package:flutter_security_app/screens/VisitorDetail.dart';
import 'package:intl/intl.dart';

class CustomCard extends StatefulWidget {
  CustomCard(
      {@required this.title,
      this.description,
      this.roomNo,
      this.visitorNo,
      this.vehicle,
      this.vehicleNo,
      this.visitingPurpose,
      this.otherPurpose,
      this.url,
      this.time,
      this.outTime,
      this.document,
      this.widget,
      this.noOfchild});

// THIS CREATES THE CARD LIST WICH CONTAINS THE DATA
  var title;
  var description;
  var roomNo;
  var visitorNo;
  var vehicle;
  var vehicleNo;
  var visitingPurpose;
  var otherPurpose;
  var url;
  var time;
  var outTime;
  DocumentSnapshot document;
  var widget;
  var noOfchild;

  @override
  _CustomCardState createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {
  String newtime;

  bool isIconVisible = true;

  @override
  Widget build(BuildContext context) {
    _iconDisable(widget.outTime);

    return Card(
      elevation: 5.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(widget.url),
        ),
        trailing: isIconVisible ? addIcon() : addText(),

        title: Text(widget.title),
        subtitle: Text(widget.description),
        // trailing: Icon(Icons.delete, color:Colors.black,),

        onTap: () {

          navigateToDetail(context);
        },
      ),
    );
  }

  void navigateToDetail(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return TaskPage(
          title: widget.title,
          description: widget.description,
          roomNo: widget.roomNo,
          visitorNo: widget.visitorNo,
          vehicle: widget.vehicle,
          vehicleNo: widget.vehicleNo,
          noOfchild: widget.noOfchild,
          visitingPurpose: widget.visitingPurpose,
          otherPurpose: widget.otherPurpose,
          url: widget.url,
          time: widget.time,
          outTime: widget.outTime,
          document: widget.document);
    }));
  }

  void _updateData(context) async {
    newtime =
        new DateFormat("dd-MM-yyyy hh:mm a").format(DateTime.now()).toString();
    await Firestore.instance
        .collection("users")
        .document(widget.widget)
        .collection('Visitors')
        .document(widget.document.documentID)
        .updateData({'outTime': newtime}).then(
            (result) => {_iconDisable(newtime)});



  }

  _iconDisable(newtime){
    setState(() {
      if(newtime == null){
        isIconVisible = true;
      }else if(newtime.toString().isEmpty){
        isIconVisible = true;
      }else {
        isIconVisible = false;
      }
    });

  }

 Widget addText() {
  return Padding(
    padding: const EdgeInsets.only(top:15.0),
    child: Container(
       child: Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
           Text("In:${widget.time}",
         textAlign: TextAlign.left,
           style: TextStyle(
             fontSize: 10,
             color: Colors.green,
           ),
           ),
           Text("Out:${widget.outTime}",
             textAlign: TextAlign.left,
             style: TextStyle(
                 fontSize: 10,
               color: Colors.red
             ),
           ),
           Text("Room No:${widget.roomNo}",
             textAlign: TextAlign.left,
             style: TextStyle(
                 fontSize: 10,
               fontWeight: FontWeight.bold,
               color: Colors.black87
             ),
           )
         ],
       ),
     ),
  );
 }

 Widget addIcon() {
    return Visibility(
      visible: isIconVisible,
      child: IconButton(
        icon: Image.asset("images/logoutpixel.png"),
        iconSize: 30,
        onPressed: () {
          setState(() {
            dialogBox(context);

          });

        },
      ),
    );
  }

 Future<void> dialogBox(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder:(BuildContext context){
      return AlertDialog(
        title:
          Text("Colock Out"),
        content: Text('Do You Want To Clock Out'),
        actions: <Widget>[
          FlatButton(
            child: Text('Yes'),
            onPressed: (){
              _updateData(context);
              Navigator.of(context).pop();
            } ,
          ),
          FlatButton(
            child: Text('No'),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    }
    );
  }
}
