import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_security_app/screens/LoginPage.dart';
import 'package:flutter_security_app/screens/VisitorList.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  initState() {
    FirebaseAuth.instance.currentUser().then((currentUser) {
      return {methodData(currentUser)};
    }).catchError((err) => print(err));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  void navigateToLoginPage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LoginPage();
    }));
  }

  methodData(FirebaseUser currentUser) {
    if (currentUser == null) {
      navigateToLoginPage();
    } else {
      Firestore.instance
          .collection("users")
          .document(currentUser.uid)
          .get()
          .then((DocumentSnapshot result) => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => VisitorList(
                        result["fname"],
                        currentUser.uid,
                      ))))
          .catchError((err) => print(err));
    }
  }
}
