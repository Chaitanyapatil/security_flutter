import 'package:flutter/material.dart';
import 'package:flutter_security_app/screens/LoginPage.dart';
import 'package:flutter_security_app/screens/SplashPage.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SecurityApp',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.deepPurple
      ),
      home: new SplashPage(),

    );
  }
}

